import styled from 'styled-components';

export const Wrapper = styled.div`
  background-color: #0f3d4c;
  height: 1480px;
  position: relative;
  padding: 100px 0;

  @media (max-width: 768px) {
    height: auto;
    padding: 80px 0;
  }

  &::before {
    content: '';
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    background: url('bg22.png') no-repeat;
    mix-blend-mode: overlay;
    background-position: right;
    background-size: contain;
  }

  h2 {
    width: 85%;
    @media (max-width: 768px) {
      width: 100%;
      font-size: 40px;
      padding-top: 20px;
    }
  }
`;

export const Content = styled.div`
  width: 43%;
  margin-top: 50px;
  margin-bottom: 70px;

  @media (max-width: 768px) {
    width: 100%;
  }

  p {
    color: #fff;
    margin-bottom: 30px;
    font-size: 18px;
    line-height: 25px;
  }

  a {
    color: #fff;
    text-decoration: none;
    font-weight: bold;
    position: relative;

    &::before {
      content: '';
      position: absolute;
      width: 100%;
      border-bottom: 6px solid transparent;
      border-image: linear-gradient(to right, #ffa050 0%, #f0633d 100%);
      border-image-slice: 1;
      bottom: 1px;
      z-index: -1;
    }
  }

  span {
    font-weight: bold;
    text-transform: uppercase;
    letter-spacing: 10px;
  }
`;

export const Images = styled.div`
  position: relative;
`;

export const Image = styled.div`
  position: absolute;
  top: ${(props) => props.top};
  right: ${(props) => props.right};
  left: ${(props) => props.left};

  &:before {
    content: '';
    position: absolute;
    top: -25px;
    left: -25px;
    right: 350px;
    bottom: 50px;
    border-left: 2px solid #f04e23;
    border-top: 2px solid #f04e23;
    display: ${(props) => (props.border ? 'block' : 'none')};
  }

  @media (max-width: 768px) {
    position: static;
    margin-bottom: 15px;

    img {
      width: 100%;
    }
  }
`;
