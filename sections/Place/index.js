import React from 'react';

import Container from '../../components/Container';
import Title from '../../components/Title';

import { Wrapper, Content, Images, Image } from './styles';

function Place() {
  return (
    <Wrapper>
      <Container>
        <Title>Una noche de diversión y aprendizaje en Lamucca de Prado </Title>
        <Content>
          <p>
            Fundado en 2010 en el corazón de Madrid,{' '}
            <strong>
              <i>el restaurante Lamucca </i>
            </strong>
            combina la elegancia de los techos altos y los grandes ventanales
            que dan a las Calles de Prado y León, con el estilo industrial
            vintage.
          </p>
          <p>
            Un ambiente divertido, moderno y agradable con una carta estupenda,
            ideal para los más diversos tipos de eventos.
          </p>
          <p>
            <span>Calle Prado 16</span> | <a href="#">www.lamucca.es</a>
          </p>
        </Content>
        <Images>
          <Image top={'-140px'} right={'0'}>
            <img src="place2.jpg"></img>
          </Image>
          <Image top={'0'} left={'0'}>
            <img src="place3.jpg"></img>
          </Image>
          <Image top={'200px'} right={'0'} border={true}>
            <img src="place1.jpg"></img>
          </Image>
        </Images>
      </Container>
    </Wrapper>
  );
}

export default Place;
