import styled from 'styled-components';

export const Wrapper = styled.section`
  background-color: #f04e23;
  height: 1315px;
  position: relative;
  z-index: 1;
  padding-top: 200px;

  @media (max-width: 425px) {
    padding-top: 100px;
  }

  @media (max-width: 768px) {
    height: auto;
    padding-bottom: 60px;
  }

  &::before {
    content: '';
    background-image: url('bg1.jpg');
    background-repeat: no-repeat;
    background-size: cover;
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 600px;
    opacity: 0.8;
    mix-blend-mode: overlay;
  }

  &::after {
    content: '';
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    background: linear-gradient(
      0deg,
      rgba(240, 78, 35, 1) 0%,
      rgba(240, 78, 35, 1) 31%,
      rgba(255, 255, 255, 0) 100%
    );
  }
`;

export const EventDate = styled.div`
  font-size: 48px;
  color: #fff;
  text-align: center;
  margin-top: 90px;
  font-family: 'Nunito Sans';
  font-weight: 900;
  font-style: italic;
  text-transform: uppercase;

  @media (max-width: 768px) {
    font-size: 30px;
    margin-top: 40px;
  }
`;

export const Time = styled.div`
  text-align: center;
  font-size: 26px;
  color: #fff;
  margin-top: 20px;
  font-family: 'Nunito Sans';
  text-transform: uppercase;
  letter-spacing: 13px;

  @media (max-width: 768px) {
    font-size: 20px;
    letter-spacing: 9px;
  }
`;

export const Subscribe = styled.div`
  text-align: center;
  margin-top: 95px;
  position: relative;

  @media (max-width: 768px) {
    margin-top: 60px;
  }

  &::before {
    content: '';
    width: 200%;
    border-bottom: 3px solid #ff8e3b;
    position: absolute;
    top: 50%;
    left: -50%;
    @media (max-width: 768px) {
      display: none;
    }
  }
`;

export const Content = styled.div`
  margin-top: 70px;
  position: relative;

  @media (max-width: 768px) {
    position: static;
  }

  h2 {
    width: 65%;

    @media (max-width: 768px) {
      width: 100%;
      font-size: 40px;
    }
  }
`;

export const Text = styled.div`
  width: 43%;
  position: absolute;
  right: 0;
  top: 190px;

  @media (max-width: 768px) {
    position: relative;
    top: 30px;
    width: 100%;
  }

  p {
    color: #fff;
    margin-bottom: 30px;
    font-size: 18px;
    line-height: 25px;
  }
`;

export const City = styled.div`
  font-size: 24px;
  font-family: 'Nunito Sans';
  color: #fff;
  text-transform: uppercase;
  letter-spacing: 20px;
  text-align: center;
  width: 320px;
  margin: 0 auto;
  position: relative;
  padding-left: 16px;

  &::before {
    content: '';
    position: absolute;
    height: 2px;
    width: 175px;
    background-color: #ffae3b;
    left: -140px;
    top: 15px;
  }

  &::after {
    content: '';
    position: absolute;
    height: 2px;
    width: 175px;
    background-color: #ffae3b;
    right: -140px;
    top: 15px;
  }
`;

export const Logo = styled.h1`
  background-color: rgba(165, 1, 0, 0.6);
  height: 105px;
  width: 780px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0 auto;
  border-radius: 52px;
  margin-bottom: 40px;

  @media (max-width: 768px) {
    max-width: 100%;
  }

  @media (max-width: 425px) {
    height: 70px;
  }

  div {
    position: relative;

    img {
      padding-top: 15px;

      @media (max-width: 425px) {
        max-width: 72px;
      }
      @media (max-width: 320px) {
        max-width: 50px;
      }

      &:last-child {
        position: absolute;
        left: 0;
        z-index: -1;
        top: 10px;
      }
    }
  }

  span {
    font-family: 'Neo Sans Std';
    font-size: 65px;
    color: #fff;
    text-transform: uppercase;
    text-shadow: 1px 4px 0px #710000;
    padding-top: 15px;

    @media (max-width: 425px) {
      padding-top: 5px;
      font-size: 30px;
    }
    @media (max-width: 320px) {
      font-size: 26px;
    }

    &:first-child {
      margin-right: 28px;
      @media (max-width: 425px) {
        margin-right: 10px;
      }
    }

    &:last-child {
      margin-left: 10px;
      @media (max-width: 768px) {
        margin-left: 5px;
      }
    }
  }
`;
