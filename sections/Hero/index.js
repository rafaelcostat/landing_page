import React from 'react';

import Container from '../../components/Container';
import Title from '../../components/Title';
import Button from '../../components/Button';

import {
  Wrapper,
  EventDate,
  Time,
  Subscribe,
  Content,
  Text,
  City,
  Logo,
} from './styles';

function Hero() {
  return (
    <Wrapper>
      <Container>
        <Logo>
          <span>Hotmart </span>
          <div>
            <img src="baloon.png" />
            <img src="baloon-base.png" />
          </div>
          <span>Talks</span>
        </Logo>

        <City>Madrid</City>
        <EventDate>Jueves 30 de Noviembre de 2017</EventDate>
        <Time>
          <p>Lamucca de Prado | 19:30h</p>
        </Time>
        <Subscribe>
          <Button>QUIERO INSCRIBIRME AHORA</Button>
        </Subscribe>
        <Content>
          <Title>
            Share knowledge, make friends, <br></br>and have fun.
          </Title>
          <Text>
            <p>
              <strong>
                <i>Hotmart Talks</i>
              </strong>{' '}
              es la oportunidad de participar en un Happy Hour y aprovechar para
              conocer a personas que se dedican al emprendimiento, los productos
              digitales y el mercado de la innovación.
            </p>
            <p>
              Ideado y organizado por Hotmart,{' '}
              <strong>
                <i>"Talks"</i>
              </strong>{' '}
              tiene lugar en bares o sitios que amalgaman happy hour con
              aprendizaje y cuenta siempre con la presencia de invitados
              especiales, proporcionando una experiencia placentera para todos.
              Es un momento dedicado a conversar, hacer amigos y crecer.
            </p>
          </Text>
        </Content>
      </Container>
    </Wrapper>
  );
}

export default Hero;
