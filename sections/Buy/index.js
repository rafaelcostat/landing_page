import React from 'react';

import Container from '../../components/Container';
import Title from '../../components/Title';

import {
  Circle,
  Button,
  EventDate,
  Price,
  Wrapper,
  Logo,
  City,
} from './styles';

function Buy() {
  return (
    <Wrapper>
      <Container>
        <Circle>
          <Title size={'76px'}>¡Compra tu billete ahora!</Title>
          <Logo>
            <span>Hotmart</span>
            <div>
              <img src="baloon.png" />
              <img src="baloon-base.png" />
            </div>
            <span>Talks</span>
          </Logo>
          <City>Madrid</City>
          <EventDate>30 de Noviembre </EventDate>
          <Price>€27,00</Price>
          <Button>COMPRAR</Button>
        </Circle>
      </Container>
    </Wrapper>
  );
}

export default Buy;
