import styled from 'styled-components';
import { darken } from 'polished';

export const Wrapper = styled.div`
  position: relative;
  height: 765px;

  @media (max-width: 768px) {
    height: auto;
    background-color: #f04e23;
  }
`;

export const Circle = styled.div`
  position: absolute;
  top: -60px;
  left: 0;
  right: 0;
  width: 885px;
  height: 885px;
  background-color: #f04e23;
  border-radius: 50%;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  @media (max-width: 768px) {
    position: static;
    border-radius: 0;
    width: 100%;
    height: auto;
    padding: 60px 0px;
  }

  &::before {
    content: '';
    position: absolute;
    border: 4px solid #ececec;
    width: 835px;
    height: 835px;
    border-radius: 50%;

    @media (max-width: 768px) {
      display: none;
    }
  }

  &::after {
    content: '';
    position: absolute;
    width: 475px;
    height: 445px;
    background: url(lines.png) no-repeat;
    left: -395px;

    @media (max-width: 768px) {
      display: none;
    }
  }

  h2 {
    width: 60%;
    margin: 0 auto;
    text-align: center;
    margin-bottom: 40px;

    @media (max-width: 768px) {
      width: 100%;
    }
  }
`;

export const Button = styled.button`
  border-radius: 32px;
  height: 64px;
  width: 398px;
  background-color: #02e202;
  border: none;
  color: #fff;
  font-size: 29px;
  cursor: pointer;
  font-family: 'Nunito Sans';
  font-weight: bold;
  z-index: 2;
  position: relative;
  transition: all 300ms;

  @media (max-width: 768px) {
    width: 100%;
    height: 50px;
  }

  &:hover {
    background-color: ${darken(0.05, '#02e202')};
  }
`;

export const EventDate = styled.div`
  color: #fff;
  font-size: 36px;
  font-family: 'Nunito Sans';
  font-weight: 900;
  font-style: italic;
  @media (max-width: 768px) {
    text-align: center;
    font-size: 32px;
  }
`;

export const Price = styled.div`
  color: #fff;
  font-size: 95px;
  font-family: 'Nunito Sans';
  font-weight: 900;
  font-style: italic;

  @media (max-width: 768px) {
    font-size: 50px;
    margin-bottom: 10px;
  }
`;

export const City = styled.div`
  font-size: 18px;
  font-family: 'Nunito Sans';
  color: #fff;
  text-transform: uppercase;
  letter-spacing: 16px;
  text-align: center;
  width: 220px;
  margin: 0 auto;
  position: relative;
  padding-left: 16px;

  &::before {
    content: '';
    position: absolute;
    height: 1px;
    width: 120px;
    background-color: #ffae3b;
    left: -120px;
    top: 15px;
  }

  &::after {
    content: '';
    position: absolute;
    height: 1px;
    width: 120px;
    background-color: #ffae3b;
    right: -120px;
    top: 15px;
  }
`;

export const Logo = styled.div`
  background-color: rgba(165, 1, 0, 0.6);
  height: 70px;
  width: 515px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0 auto;
  border-radius: 35px;
  margin-bottom: 25px;

  @media (max-width: 768px) {
    max-width: 100%;
  }

  @media (max-width: 425px) {
    height: 70px;
  }

  div {
    position: relative;

    img {
      padding-top: 10px;
      width: 100px;

      @media (max-width: 425px) {
        max-width: 72px;
      }
      @media (max-width: 320px) {
        max-width: 50px;
      }

      &:last-child {
        position: absolute;
        left: 0;
        z-index: -1;
        top: 10px;
      }
    }
  }

  span {
    font-family: 'Neo Sans Std';
    font-size: 43px;
    color: #fff;
    text-transform: uppercase;
    text-shadow: 1px 3px 0px #710000;
    padding-top: 9px;

    @media (max-width: 425px) {
      padding-top: 5px;
      font-size: 30px;
    }
    @media (max-width: 320px) {
      font-size: 26px;
    }

    &:first-child {
      margin-right: 18px;
      @media (max-width: 425px) {
        margin-right: 10px;
      }
    }

    &:last-child {
      margin-left: 10px;
      @media (max-width: 768px) {
        margin-left: 5px;
      }
    }
  }
`;
