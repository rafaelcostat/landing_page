import React from 'react';

import {
  Wrapper,
  Person,
  Image,
  ImageWrapper,
  Name,
  Subscribe,
} from './styles';

import Container from '../../components/Container';
import Button from '../../components/Button';

function People() {
  return (
    <Wrapper>
      <Container>
        <Person>
          <ImageWrapper>
            <Image img={'p1.jpg'}></Image>
          </ImageWrapper>
          <Name>Raul Maraña</Name>
          <p>
            Business Development Leader <br></br>
            <strong>Europe | Hotmart</strong>
          </p>
          <a href="#">ver más</a>
        </Person>
        <Person>
          <ImageWrapper>
            <Image img={'p2.jpg'}></Image>
          </ImageWrapper>
          <Name>Alejandro</Name>
          <Name>Novás</Name>
          <p>
            Co-fundador de Mundo Entrenamiento y fundador de{' '}
            <strong>Vivir de tu Pasión</strong>
          </p>
          <a href="#">ver más</a>
        </Person>
        <Person>
          <ImageWrapper>
            <Image img={'p3.jpg'}></Image>
          </ImageWrapper>
          <Name>Borja Montón</Name>
          <p>
            Creador de contenido, Ilusionista <br></br>profesional y joven
            emprendedor
          </p>
          <a href="#">ver más</a>
        </Person>
      </Container>
      <Subscribe>
        <Button>QUIERO INSCRIBIRME AHORA</Button>
      </Subscribe>
    </Wrapper>
  );
}

export default People;
