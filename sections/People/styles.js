import styled from 'styled-components';

export const Wrapper = styled.section`
  padding: 80px 0 110px;

  > div {
    display: flex;
    justify-content: space-between;

    @media (max-width: 768px) {
      flex-wrap: wrap;
    }
  }
`;

export const Person = styled.div`
  width: 33%;

  @media (max-width: 768px) {
    width: 100%;
    margin-bottom: 50px;
    display: flex;
    flex-direction: column;
    align-items: center;
    text-align: center;
  }

  p {
    margin-top: 10px;
    font-size: 18px;
  }

  a {
    display: block;
    margin-top: 15px;
    color: #f1653e;
    text-decoration: none;
    font-size: 18px;
    font-weight: bold;
    background: url('more.png') no-repeat;
    padding-left: 30px;
    background-position: 0px 3px;
  }
`;

export const Name = styled.div`
  font-size: 47px;
  font-family: 'Nunito Sans';
  font-weight: 900;
  font-style: italic;
  color: #15100f;
  position: relative;
  display: inline-block;
  padding: 0 10px;
  line-height: 45px;

  &:before {
    content: '';
    position: absolute;
    width: 100%;
    height: 8px;
    border-bottom: 12px solid transparent;
    border-image: linear-gradient(to right, #ffa050 0%, #f0633d 100%);
    border-image-slice: 1;
    bottom: 2px;
    z-index: -1;
    left: 0;
  }
`;

export const Image = styled.div`
  background: url(${(props) => props.img}) no-repeat;
  height: 260px;
  width: 260px;
  border-radius: 50%;
  background-size: cover;
`;

export const ImageWrapper = styled.div`
  background: linear-gradient(to right, #ff9f50, #f0633d);
  padding: 12px;
  width: 260px;
  height: 260px;
  border-radius: 50%;
  margin-bottom: 50px;
`;

export const Subscribe = styled.div`
  position: relative;

  button {
    position: absolute;
    margin: 0 auto;
    left: 0;
    right: 0;
    bottom: -140px;
    z-index: 4;
  }
`;
