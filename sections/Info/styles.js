import styled from 'styled-components';

export const Wrapper = styled.div`
  height: 700px;
  background-color: #0f3d4c;
  padding-top: 130px;
  position: relative;

  @media (max-width: 768px) {
    height: auto;
    padding-top: 60px;
  }

  &::before {
    content: '';
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    background: url('bg3.jpg') no-repeat;
    mix-blend-mode: overlay;
    background-position: center;
    background-size: cover;
  }

  > div {
    display: flex;

    @media (max-width: 768px) {
      flex-wrap: wrap;
    }
  }

  h2 {
    width: 50%;
    @media (max-width: 768px) {
      width: 100%;
      font-size: 40px;
      text-align: center;
    }
  }
`;

export const Content = styled.div`
  width: 48%;
  padding-left: 160px;
  padding-top: 135px;

  @media (max-width: 1024px) {
    padding-left: 60px;
  }

  @media (max-width: 768px) {
    width: 100%;
    padding: 50px 0;
  }

  p {
    color: #fff;
    margin-bottom: 35px;
    line-height: 25px;
    font-size: 18px;
  }
`;
