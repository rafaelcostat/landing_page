import React from 'react';

import Container from '../../components/Container';
import Title from '../../components/Title';

import { Wrapper, Content } from './styles';

function Info() {
  return (
    <Wrapper>
      <Container>
        <Title>Idealización y realización</Title>
        <Content>
          <p>
            <strong>
              <i>Hotmart</i>
            </strong>{' '}
            es líder en la venta y distribución de infoproductos en toda
            Latinoamérica y ha permitido que miles de personas trabajen con lo
            que más aman, vendiendo y divulgando productos digitales en todo el
            mundo.
          </p>
          <p>
            Debido al interés del mercado europeo por el segmento de productos
            digitales, Hotmart posee una sede en Madri y actúa también en
            Holanda y Francia. Ahora, la empresa sintió que tiene mucho para
            compartir con los profesionales locales y decidió promover un
            encuentro entre ellos:
            <strong>
              especialistas y personas de diferentes áreas que, al igual que tú,
              están interesadas en aprender y compartir sus experiencias para
              darle un nuevo sentido a sus trayectorias profesionales.
            </strong>
          </p>
        </Content>
      </Container>
    </Wrapper>
  );
}

export default Info;
