import styled from 'styled-components';
import { darken } from 'polished';

export const Wrapper = styled.div`
  background-color: #c7c7c7;
  padding: 140px 0;

  @media (max-width: 768px) {
    padding: 60px 0;
  }

  h2 {
    color: #565656;
    width: 60%;

    @media (max-width: 768px) {
      width: 100%;
    }
  }

  form {
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    justify-content: space-evenly;
    margin-top: 80px;

    @media (max-width: 768px) {
      margin-top: 30px;
    }
  }
`;

export const Content = styled.div`
  display: flex;

  @media (max-width: 768px) {
    flex-wrap: wrap;
  }

  p {
    color: #565656;
    font-weight: bold;
    font-size: 18px;
    line-height: 25px;
    width: 45%;
    margin-top: 120px;

    @media (max-width: 768px) {
      width: 100%;
      margin-top: 30px;
    }
  }
`;

export const Form = styled.form``;

export const InputWrapper = styled.div`
  width: 480px;

  @media (max-width: 768px) {
    width: 100%;
    margin-bottom: 30px;
  }

  input {
    padding: 24px;
    font-size: 16px;
    border: 2px solid #575757;
    background-color: #ebebeb;

    @media (max-width: 768px) {
      padding: 20px;
    }
  }

  label {
    font-weight: bold;
    font-size: 18px;
    margin-bottom: 10px;
  }

  span {
    font-size: 16px;
    font-weight: bold;
    position: absolute;
    bottom: -30px;
    color: #ec2424;
  }

  > div {
    display: flex;
    flex-direction: column;
    position: relative;

    &.error {
      input {
        border-color: #ec2424;
      }
      label {
        color: #ec2424;
      }
    }
  }
`;

export const ButtonWrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  margin-top: 50px;

  button {
    font-family: 'Nunito Sans';
    font-weight: bold;
    font-size: 22px;
    border: none;
    padding: 10px 60px;
    color: #fff;
    background-color: #565656;
    border-radius: 25px;
    transition: all 300ms;

    &:hover {
      background-color: ${darken(0.05, '#565656')};
    }
  }
`;

export const CheckWrapper = styled.div`
  margin-top: 65px;

  @media (max-width: 768px) {
    margin-top: 20px;
  }
`;
