import React, { useRef, useState } from 'react';
import { Form } from '@unform/web';
import * as Yup from 'yup';

import Input from '../../components/Input';
import Container from '../../components/Container';
import Title from '../../components/Title';
import Checkbox from '../../components/Checkbox';

import {
  Wrapper,
  Content,
  InputWrapper,
  ButtonWrapper,
  CheckWrapper,
} from './styles';

function Contact() {
  const formRef = useRef(null);
  const [cavalo, setChecked] = useState(false);

  async function handleSubmit(data) {
    try {
      // Remove all previous errors
      formRef.current.setErrors({});
      const schema = Yup.object().shape({
        email: Yup.string().email().required('Este campo es obligatorio'),
        name: Yup.string().min(3).required('Este campo es obligatorio'),
      });
      await schema.validate(data, {
        abortEarly: false,
      });
      // Validation passed
      console.log(data);
    } catch (err) {
      const validationErrors = {};
      if (err instanceof Yup.ValidationError) {
        err.inner.forEach((error) => {
          validationErrors[error.path] = error.message;
        });
        formRef.current.setErrors(validationErrors);
      }
    }
  }

  function handleCheckboxChange(e) {
    console.log('cacac');
    setChecked(event.target.checked);
  }

  return (
    <Wrapper>
      <Container>
        <Content>
          <Title>¡No te pierdas las novedades!</Title>
          <p>
            Suscríbete a nuestra newsletter, habilita las notificaciones y
            recibe el mejor contenido sobre emprendimiento y marketing digital.
          </p>
        </Content>
        <Form ref={formRef} onSubmit={handleSubmit}>
          <InputWrapper>
            <Input
              name="name"
              label="Nombre"
              type="text"
              placeholder="Informa tu nombre completo"
            />
          </InputWrapper>
          <InputWrapper>
            <Input
              name="email"
              label="Email"
              type="email"
              placeholder="Informa tu correo electrónico "
            />
          </InputWrapper>
          <CheckWrapper>
            <Checkbox
              label="Sí, acepto recibir los contenidos de Hotmart y entiendo que puedo
              darme de baja en cualquier momento."
              checked={cavalo}
              onChange={handleCheckboxChange}
            />
          </CheckWrapper>
          <ButtonWrapper>
            <button type="submit">¡INSCRÍBEME!</button>
          </ButtonWrapper>
        </Form>
      </Container>
    </Wrapper>
  );
}

export default Contact;
