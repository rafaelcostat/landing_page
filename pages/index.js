import React from 'react';
import Head from 'next/head';

import Footer from '../components/Footer';

import Hero from '../sections/Hero';
import People from '../sections/People';
import Place from '../sections/Place';
import Buy from '../sections/Buy';
import Info from '../sections/Info';
import Contact from '../sections/Contact';

export default function Home() {
  return (
    <>
      <Head>
        <title>Hotmart Talks</title>
        <link rel="icon" href="/favicon.png" />
        <link
          href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,400;0,700;1,400;1,800&display=swap"
          rel="stylesheet"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,400;0,700;1,900&display=swap"
          rel="stylesheet"
        />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Head>
      <main>
        <Hero></Hero>
        <People></People>
        <Place></Place>
        <Buy></Buy>
        <Info />
        <Contact />
      </main>
      <footer>
        <Footer />
      </footer>
    </>
  );
}
