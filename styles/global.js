import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`

  @font-face {
    font-family: 'Neo Sans Std';
    src: url('fonts/NeoSansStd-MediumItalic.woff2') format('woff2'),
        url('fonts/NeoSansStd-MediumItalic.woff') format('woff');
    font-weight: 500;
    font-style: italic;
  }

  * {
    margin: 0;
    padding: 0;
  }

  *:focus {
    outline: none;
  }


  html, body {
    -webkit-font-smoothing: antialiased;
    overflow-x: hidden;
    font-family: "Open Sans";
  }

  button {
    cursor: pointer;
  }
`;
