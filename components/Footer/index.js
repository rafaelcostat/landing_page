import React from 'react';
import { FaInstagram, FaFacebookF, FaTwitter, FaYoutube } from 'react-icons/fa';

import Container from '../Container';
import { Wrapper, Social, Logo, Mail } from './styles';

function Footer() {
  return (
    <Wrapper>
      <Container>
        <Mail>
          <strong>¡Habla con nosotros!</strong>
          <p>
            ¿Dudas?{' '}
            <span>
              Envía un email a <a href="#">talks@hotmart.com</a>
            </span>
          </p>
        </Mail>
        <Logo>
          <a>
            <img src="hotmart.png" />
          </a>
        </Logo>
        <Social>
          <li>
            <a href="#" target="_blank">
              <FaFacebookF color="#fff" size={25} />
            </a>
          </li>
          <li>
            <a href="#" target="_blank">
              <FaInstagram color="#fff" size={25} />
            </a>
          </li>
          <li>
            <a href="#" target="_blank">
              <FaTwitter color="#fff" size={25} />
            </a>
          </li>
          <li>
            <a href="#" target="_blank">
              <FaYoutube color="#fff" size={28} />
            </a>
          </li>
        </Social>
      </Container>
    </Wrapper>
  );
}

export default Footer;
