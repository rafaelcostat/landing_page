import styled from 'styled-components';

export const Wrapper = styled.div`
  background-color: #2e2e2e;
  height: 225px;

  @media (max-width: 768px) {
    height: auto;
    padding: 30px 0;
  }

  > div {
    display: flex;
    align-items: center;
    justify-content: space-between;
    height: 100%;

    @media (max-width: 768px) {
      flex-wrap: wrap;
      justify-content: center;
    }
  }
`;

export const Social = styled.ul`
  list-style: none;
  display: flex;

  li {
    width: 60px;
    height: 60px;
    background-color: #202020;
    border-radius: 50%;
    display: flex;
    align-items: center;
    justify-content: center;
    margin-left: 20px;

    @media (max-width: 320px) {
      width: 50px;
      height: 50px;
    }

    svg {
      transition: all 300ms;
    }

    a {
      line-height: 1;
    }

    &:hover {
      svg {
        color: #ec4924 !important;
      }
    }
  }
`;

export const Logo = styled.div`
  @media (max-width: 768px) {
    margin-bottom: 20px;
  }
`;

export const Mail = styled.div`
  font-style: italic;

  @media (max-width: 768px) {
    margin-bottom: 20px;
    text-align: center;
  }

  strong {
    font-size: 20px;
    color: #9e9e9e;
    font-weight: 900;
    font-style: italic;
  }

  p {
    color: #9e9e9e;
    font-size: 18px;
    font-weight: 400;
  }

  a {
    color: #fff;
  }

  span {
    color: #fff;
  }
`;
