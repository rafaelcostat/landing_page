import styled from 'styled-components';

const Container = styled.div`
  max-width: 1160px;
  margin: 0 auto;
  padding: 0 15px;
  position: relative;
  z-index: 2;

  @media (max-width: 768px) {
    padding: 0 15px !important;
  }
`;

export default Container;
