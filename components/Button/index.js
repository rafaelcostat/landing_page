import styled from 'styled-components';
import { darken } from 'polished';

const Button = styled.button`
  border-radius: 32px;
  height: 64px;
  width: 498px;
  background-color: #02e202;
  border: 2px solid #fff;
  color: #fff;
  font-size: 29px;
  cursor: pointer;
  font-family: 'Nunito Sans';
  font-weight: bold;
  z-index: 2;
  position: relative;
  transition: all 300ms;

  @media (max-width: 768px) {
    width: 90%;
    font-size: 19px;
  }

  &:hover {
    background-color: ${darken(0.05, '#02e202')};
  }
`;

export default Button;
