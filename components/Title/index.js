import styled from 'styled-components';

const Title = styled.h2`
  font-size: ${(props) => (props.size ? props.size : '85px')};
  font-family: 'Nunito Sans';
  font-weight: 900;
  font-style: italic;
  color: #fff;
  line-height: 110%;

  @media (max-width: 1024px) {
    font-size: 78px;
  }

  @media (max-width: 768px) {
    font-size: 40px;
    text-align: center;
  }
`;

export default Title;
