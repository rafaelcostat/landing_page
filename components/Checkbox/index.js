import React from 'react';

import { Container } from './styles';

function Checkbox({ label, checked, onChange }) {
  return (
    <Container>
      {label}
      <input type="checkbox" checked={checked} onChange={onChange} />
      <span></span>
    </Container>
  );
}

export default Checkbox;
