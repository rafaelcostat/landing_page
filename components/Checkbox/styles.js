import styled from 'styled-components';

export const Container = styled.label`
  display: block;
  position: relative;
  padding-left: 45px;
  padding-top: 3px;
  cursor: pointer;
  font-size: 16px;
  user-select: none;

  &:hover input ~ .span {
    background-color: #ccc;
  }

  input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    height: 0;
    width: 0;
  }

  input:checked ~ span::after {
    display: block;
  }

  span {
    position: absolute;
    top: 0;
    left: 0;
    height: 25px;
    width: 25px;
    background-color: #ebebeb;
    border: 2px solid #565656;

    &::after {
      content: '';
      left: 8px;
      top: 3px;
      width: 6px;
      height: 12px;
      position: absolute;
      border: solid #565656;
      border-width: 0 3px 3px 0;
      transform: rotate(45deg);
      display: none;
    }
  }
`;
